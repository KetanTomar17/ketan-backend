const mongoose = require('mongoose');
const schema = mongoose.Schema;

//create schema 
var employeeSchema = new schema({
        
        email: {
            type: String,
            required: true
        },
        name: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
    })
module.exports = employee = mongoose.model('employees',employeeSchema);