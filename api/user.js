const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const gravatar = require('gravatar');
const jwt = require('jsonwebtoken');
const keys = require('../config/key')

// +++++ load user model +++++
const Employee = require('../models/Employee');

// @routes  GET /api/user/test
// @desc    Test usrs
// @access  public
router.get('/test',(req,res)=>{
    res.json({
        msg : 'user worked'
    })
});

// @routes  POST /api/user/register
// @desc    register user
// @access  public
router.post('/register', (req,res)=>{
    Employee.findOne({ email: req.body.email}).then(employee =>{
        if(employee){            
            return res.status(400).json({ email: 'Email already exist'});
        }
        else{                    
            const employee = new Employee({
            name: req.body.name,
            email: req.body.email,
            password: req.body.password
            })
            bcrypt.genSalt(10, (err,salt) =>{
                bcrypt.hash(employee.password,salt,(err,hash)=>{
                    if(err) throw err;
                        employee.password = hash;
                        employee.save().then(
                        employee => res.json(employee)
                    ).catch(err => console.log(err));
                    })
                })
        }
});
}); 
   
// @routes  GET /api/user/login
// @desc  login user
// @access  public
router.post('/login', (req,res) => {
    const email = req.body.email;
    const password = req.body.password;
    
    // check user email
    Employee.findOne({email}).then(employee =>{
    if(!employee){
        return res.status(404).json({email:'incorrect email id'});
    }
    // check password
        bcrypt.compare(password, employee.password).then(isMatch =>{
        if (isMatch){
    //  JWT Token 
         const payload = { id: employee.id , name: employee.name }; //create jwt payload
    //  Sign Token
        jwt.sign(
            payload,
            keys.secretorkey,
            { expiresIn: 3600 },
            (err, token) => {
                res.json({
                    success: true,
                    token: 'welcome ' + token,
                    msg: 'login successful'
                });
            });
        }else{
            return res.status(400).json({password: 'incorrect password'});
        }
     });
   });
});

// @routes  GET /api/user/fetch
// @desc  fetch data from database
// @access  public
router.post('/fetch',(req,res) => { 
        Employee.find({email: req.body.email}).then(employee =>{
            if(employee){
                console.log(employee);}
           
     });
 });
// @routes  GET /api/user/test/id
// @desc  id 
// @access  public
router.get('/test/:id',(req,res) => {
      res.send('id'+ req.params.id)
});  
 
module.exports = router;